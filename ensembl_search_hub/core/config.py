"""
.. See the NOTICE file distributed with this work for additional information
   regarding copyright ownership.
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import os


PAGE = os.getenv("PAGE", 1)
PER_PAGE = os.getenv("PAGE", 100)
SOLR_SERVER = os.getenv("SOLR_SERVER", "ensembl-solr-svc:8983")
SOLR_COLLECTION = os.getenv("SOLR_COLLECTION", "genome_search_v1")
EBI_SEARCH_URL = os.getenv(
    "EBI_SEARCH_URL", "https://wwwdev.ebi.ac.uk/ebisearch/ws/rest/ensemblNext"
)

IDENTIFIERS_ORG_BASE_URL: str = os.getenv(
    "IDENTIFIERS_ORG_BASE_URL", default="https://identifiers.org/"
)

ASSEMBLY_URLS = {}
ASSEMBLY_URLS['GCA'] = IDENTIFIERS_ORG_BASE_URL + "insdc.gca/"
ASSEMBLY_URLS['GCF'] = IDENTIFIERS_ORG_BASE_URL + "refseq.gcf/"

