from fastapi import APIRouter
from ensembl_search_hub.solr_search_app.utils.solr_search import *

from ensembl_search_hub.solr_search_app.models.gene_search import SearchQuery, SearchResult, ResolverSearchQuery, ResolverSearchResult
from ensembl_search_hub.solr_search_app.utils.solr_client import SOLRClient

from ensembl_search_hub.core.config import SOLR_SERVER, SOLR_COLLECTION

solr = SOLRClient(SOLR_SERVER, SOLR_COLLECTION)

router = APIRouter()

@router.post("/genes", response_model=SearchResult)
async def get_search_results(search_query: SearchQuery):
    """
    Query SOLR with the search string and additional parameter and transform result
    as expected by the client
    """
    solr_query_input = await transform_request_for_solr(search_query)
    search_result = await solr.get_search_results(solr_query_input)
    meta = {
        "total_hits": search_result["response"]["numFound"],
        "page": search_query.page,
        "per_page": search_query.per_page,
    }
    transformed_search_result_docs = await transform_search_result(search_result)
    response = SearchResult(meta=meta, matches=transformed_search_result_docs)
    return response

# to solve resolver requests
@router.post("/stable-id", response_model=ResolverSearchResult)
async def get_resolver_search_results(search_query: ResolverSearchQuery):
    """
    Query SOLR to return genome ids when searched with an unversioned stable id
    """
    solr_query_input = await transform_request_for_resolver(search_query)
    search_result = await solr.get_search_results(solr_query_input)
    transformed_search_result_docs = await transform_search_result(search_result)
    # transformed_search_result_docs = search_result
    response = ResolverSearchResult(matches=transformed_search_result_docs)
    return response