from fastapi import APIRouter
from ensembl_search_hub.solr_search_app.resources import gene_search_view


router = APIRouter()

router.include_router(
    gene_search_view.router, tags=["gene search"]
)
