from typing import Optional, List, Literal
from pydantic import BaseModel, validator


class SearchQuery(BaseModel):
    query: str
    genome_ids: List[str]  # = Query(..., min_length=1)
    page: Optional[int] = 1
    per_page: Optional[int] = 100

    @validator("query")
    def quote_search_term(cls, query):
        return f'symbol:{query}* OR  alternative_symbols:{query}* stable_id:{query} OR unversioned_stable_id:{query}'

class SearchResultMeta(BaseModel):
    total_hits: int
    page: int
    per_page: int


class Location(BaseModel):
    start: int
    end: int


class Region(BaseModel):
    name: str


class Strand(BaseModel):
    code: str


class Slice(BaseModel):
    location: Location
    region: Region
    strand: Strand


class SearchResultDoc(BaseModel):
    type: str
    stable_id: str
    unversioned_stable_id: str
    biotype: str
    symbol: str = None
    name: str
    genome_id: str
    transcript_count: int
    slice: Slice


class SearchResult(BaseModel):
    meta: SearchResultMeta
    matches: List[SearchResultDoc] = []

class ResolverSearchQuery(BaseModel):
    stable_id: str
    type: Optional[Literal['Gene']]
    gca: Optional[str]
    per_page: Optional[int] = 10

    @validator("type", pre=True)
    def normalize_type(cls, type):
        return type.capitalize()

class ResolverSearchMatch(BaseModel):
    genome_id: str
    unversioned_stable_id: str
class ResolverSearchResult(BaseModel):
    matches: List[ResolverSearchMatch] = []