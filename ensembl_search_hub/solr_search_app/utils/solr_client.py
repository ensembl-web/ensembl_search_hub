"""
.. See the NOTICE file distributed with this work for additional information
   regarding copyright ownership.
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import asyncio, aiohttp
import json


class SOLRClient:
    def __init__(self, server_url, collection):
        self.server = server_url
        self.collection = collection
        self.search_url = f"{self.server}/{self.collection}/select?"

    def _prepare_search_request(self, solr_query_input):
        query_params = "&".join(f"{k}={v}" for k, v in solr_query_input.items())
        request_string = f"{self.search_url}{query_params}"
        return request_string

    async def get_search_results(self, solr_query_input):
        solr_request = self._prepare_search_request(solr_query_input)
        headers = {"Accept": "application/json"}
        search_results = None
        async with aiohttp.ClientSession() as session:
            async with session.get(solr_request, headers=headers) as response:
                if response.status == 200:
                    search_results = await response.json(content_type=None)
        return search_results


async def main():
    # Change search_q, solr_url, collection, genome_ids to whatever is relevant
    solr_url = "http://hh-rke-wp-webadmin-40-worker-1.caas.ebi.ac.uk:30945/solr"
    collection = "beta3"
    e2020_solr = SOLRClient(solr_url, collection)
    search_q = "BRCA"
    resp1 = await e2020_solr.get_search_results(search_q)
    print(resp1["response"]["numFound"])
    resp2 = await e2020_solr.get_search_results(
        search_q, ["a7335667-93e7-11ec-a39d-005056b38ce3"]
    )
    print(resp2["response"]["numFound"])
    resp3 = await e2020_solr.get_search_results(
        search_q, ["a7335667-93e7-11ec-a39d-005056b38ce3", "0cbc227a-7179-43c2-8061-a50d94423c4d"]
    )
    print(resp3["response"]["numFound"])


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
