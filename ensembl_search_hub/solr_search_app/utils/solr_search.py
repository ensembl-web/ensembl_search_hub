"""
.. See the NOTICE file distributed with this work for additional information
   regarding copyright ownership.
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


async def transform_request_for_solr(query_input):
    """
    Transform client request parameter to the parameter expected by the SOLR
    """
    solr_query_params = {}
    wt = "json"
    q = query_input.query
    rows = query_input.per_page
    start = (query_input.page - 1) * rows
    genome_ids = query_input.genome_ids

    solr_query_params["q"] = q
    solr_query_params["start"] = start
    solr_query_params["rows"] = rows
    solr_query_params["wt"] = wt

    fq = None

    if genome_ids:
        fq = " OR ".join(query_input.genome_ids)
        solr_query_params["fq"] = f"genome_id:({fq})"

    return solr_query_params


async def collate_slice(search_doc):
    """
    Create Slice Object from the raw solr result document
    """
    collated_slice = {"location": {}, "region": {}, "strand": {}}
    collated_slice["location"]["start"] = search_doc["slice.location.start"]
    collated_slice["location"]["end"] = search_doc["slice.location.end"]
    collated_slice["region"]["name"] = search_doc["slice.region.name"]
    collated_slice["strand"]["code"] = search_doc["slice.strand.code"]
    return collated_slice


async def transform_search_result(search_result):
    """
    Transform search result returned from SOLR in to the format expected by the client
    """
    transformed_search_result_docs = []
    for search_result_doc in search_result["response"]["docs"]:
        collated_slice = await collate_slice(search_result_doc)
        search_result_doc["slice"] = collated_slice
        # Remove unwanted fields
        del search_result_doc["slice.region.name"]
        del search_result_doc["slice.location.start"]
        del search_result_doc["slice.location.end"]
        del search_result_doc["slice.strand.code"]
        del search_result_doc["id"]
        del search_result_doc["_version_"]
        transformed_search_result_docs.append(search_result_doc)
    return transformed_search_result_docs


async def transform_request_for_resolver(query_input):
    """
    Transform client request parameter to the parameter expected by the SOLR
    """
    solr_query_params = {}
    wt = "json"
    rows = query_input.per_page
    solr_query_params["rows"] = rows
    solr_query_params["wt"] = wt
    solr_query_params["q"] = f"unversioned_stable_id:{query_input.stable_id} OR stable_id:{query_input.stable_id}"

    if query_input.type:
        solr_query_params["fq"] = f"type:({query_input.type})"

    if query_input.gca:
        solr_query_params["fq"] += f" AND gca:({query_input.gca})"

    return solr_query_params
