from fastapi import APIRouter, responses

from ensembl_search_hub.ebi_search_app.utils.ebi_search import (
    SpeciesTaxonomyLookup,
    GenomeSearch,
)

router = APIRouter()


@router.get("/genomes", name="genome_search")
def genome_search(query: str = None, species_taxonomy_id: str = None):

    if query and not species_taxonomy_id:
        response = GenomeSearch(query=query).get_result()
    elif species_taxonomy_id:
        response = SpeciesTaxonomyLookup(
            species_taxonomy_id=species_taxonomy_id
        ).get_result()
    else:
        response = {"error": 400, "response": "400 Bad Request"}
    
    if "error" in response:
        return responses.JSONResponse(response, status_code=response.get("error",500))
    
    return responses.JSONResponse(response)
