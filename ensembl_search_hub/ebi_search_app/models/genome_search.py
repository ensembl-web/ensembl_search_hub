from typing import List, Optional
from pydantic import BaseModel, Field, PrivateAttr, validator, root_validator
from ensembl_search_hub.core.config import ASSEMBLY_URLS


class Meta(BaseModel):
    total_count: int = 0

class Assembly(BaseModel):
    accession_id: str
    name: str
    url: str = None

    @validator("*", pre=True, always=True)
    def convert_list_to_field_value(cls, value):
        # EBI search results are returned as list or empty list.
        if type(value) == list:
            value = next(iter(value), None)
        return value

    @validator("url", always=True)
    def generate_url(cls, value):
        if value.startswith("GCA"):
            return ASSEMBLY_URLS['GCA'] + value
        if value.startswith("GCF"):
            return ASSEMBLY_URLS['GCF'] + value
        return None


class Type(BaseModel):
    kind: str
    value: str

    @validator("*", pre=True, always=True)
    def convert_list_to_field_value(cls, value):
        # Each EBI search result is returned as a list or an empty list.
        if type(value) == list:
            value = next(iter(value), None)
        return value


class Genome(BaseModel):
    genome_id: str = Field(alias="genome_uuid")
    genome_tag: str = Field(alias="url_name", default=None)
    species_taxonomy_id: str = Field(alias="species_taxonomy_id", default=None)
    common_name: str = Field(alias="common_name", default=None)
    scientific_name: str = Field(alias="scientific_name")
    type: Type = None
    assembly: Assembly
    is_reference: bool = Field(alias="is_reference", default=False)
    coding_genes_count: int = Field(alias="coding_genes")
    contig_n50: int = Field(alias="n50", default=None)
    has_variation: bool = Field(alias="has_variation", default=False)
    has_regulation: bool = Field(alias="has_regulation", default=False)
    annotation_provider: str = Field(alias="annotation_provider")
    organism_id: str = Field(alias="organism_id")
    annotation_method: str = Field(alias="annotation_method")
    rank: int = Field(alias="rank", default=None)

    @root_validator(pre=True)
    def generate_objects(cls, values):
        values["assembly"] = {
            "accession_id": values.get("assembly_accession", None),
            "name": values.get("assembly", None),
            "url": values.get("assembly_accession", None),
        }
        if values.get("type_type", None):
            values["type"] = {
                "kind": values.get("type_type", None),
                "value": values.get("type_value", None),
            }
        return values

    @validator("*", pre=True, always=True)
    def convert_list_to_field_value(cls, value):
        # Each EBI search result is returned as a list or an empty list.
        if type(value) == list:
            value = next(iter(value), None)
        return value

    @validator("genome_tag", "common_name", "contig_n50", "rank")
    def convert_to_null(cls, value):
        # Each EBI search returns empty string instead of null
        if value == "":
            return None
        return value


class GenomeSearchResult(BaseModel):
    matches: List[Genome] = []
    meta: Meta
    
    def sort(self, ebi_search_results: List[Genome]): 
        """Return a sorted list of genomes.
        Sorting is done by multiple fields in a specific order. 
        The multiple sorts leverage pythons > only comparison to ensure each sort order is preserved then the next sort doesn't apply
        String values are moved to lower as comparison is done on charactor value and A-Z are lower value than a-z
        z is used when a string value can be None, this prevents the sort from failing and places any empty strings at the bottom of the sort
        """
        sorts = [
            (lambda g: g.rank,True), # Order by highest rank
            (lambda g: 1 if g.is_reference else 0,True), # Bring any assembly with is_reference == True to the top of the list 
            (lambda g: g.common_name.lower() if g.common_name else 'z',False), #order by common name
            (lambda g: g.scientific_name.lower(),False), #order by scientific name
            (lambda g: g.type.value.lower() if g.type and g.type.value else 'z',False), # Order by type.value, putting Non values at the bottom of the list 
            (lambda g: g.type.kind.lower() if g.type and g.type.kind else 'z',False), # Order by type.kind, putting None values at the bottom of the list
            (lambda g: g.organism_id,False), # Ensure that related assemblies are grouped
        ]
        for key, reverse in reversed(sorts): #Sorts are applied in reverse order to ensure that the first sort is the most important sort
            ebi_search_results.sort(key=key, reverse=reverse)

        return ebi_search_results

    def __init__(self, ebi_search_result: dict, **data):
        _ebi_search_result: list = []
        for search_result in ebi_search_result.get("entries", None):
            search_result = search_result["fields"]
            _ebi_search_result.append(Genome(**search_result))

        data["matches"] = self.sort(_ebi_search_result)
        data["meta"] = Meta(total_count=ebi_search_result.get("hitCount", 0))

        super().__init__(**data)
