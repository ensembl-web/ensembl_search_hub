import regex
from urllib import parse
import requests

from ensembl_search_hub.core.config import EBI_SEARCH_URL
from ensembl_search_hub.ebi_search_app.models.genome_search import GenomeSearchResult


class EBISearch:
    def __init__(self, query):
        self.FIXED_QUERY_PAIRS = {
            "format": "json",
            "size": "300",
            "fields": "common_name,scientific_name,type_type,species_type,assembly,assembly_accession,coding_genes,n50,has_variation,has_regulation,annotation_provider,annotation_method,genome_uuid,url_name,tol_id,is_reference,species_taxonomy_id,organism_id,rank,type_value",
        }
        self.SPLIT_RULE = r"[\-\_\.\,\/]"
        self.OMIT = ["GCA"]
        self.SPECIAL = ["GCA"]
        self.raw_query = query

    def format_query(self):
        terms = [t for t in self.raw_query.split(" ") if len(t) > 1]
        processed_terms = []
        for term in terms:
            term_parts = regex.split(self.SPLIT_RULE, term)
            wild_term = [
                f"{term}*"
                for term in term_parts
                if len(term) >= 3 and term.upper() not in self.OMIT
            ]
            clean_term = term.replace("/", "\/")
            if len(wild_term) > 0: 
                processed_terms.append(f"{clean_term} OR ({' AND '.join(wild_term)})")
            else:
                processed_terms.append(f"{clean_term}")
        return " AND ".join(processed_terms)

    def build_url(self):
        query = self.format_query()
        args = [
            f"{key}={parse.quote(value)}"
            for key, value in self.FIXED_QUERY_PAIRS.items()
        ]
        args.append(f"query={parse.quote(query)}")
        return f"{EBI_SEARCH_URL}?{'&'.join(args)}"

    def get_result(self):
        url = self.build_url()
        result = requests.get(url)
        if result.status_code == 200:
            return result.json()
        else:
            return {"error": result.status_code, "response": result.text}


class GenomeSearch(EBISearch):
    def __init__(self, query=""):
        self.query = query
        super().__init__(query=query)

    def format_query(self):
        TEST = r'(?<!' + '|'.join(self.SPECIAL) + ')[\-\_\.\,\/]'
        terms = [t for t in self.raw_query.split(" ") if len(t) > 1]
        processed_terms = []
        for term in terms:
            term_parts = regex.split(TEST, term)
            wild_term = [f"{t}*" for t in term_parts if len(t) >= 3]
            clean_term = term.replace('/', '\/')
            if len(wild_term) > 0: 
                processed_terms.append(f"{clean_term} OR ({' AND '.join(wild_term)})")
            else:
                processed_terms.append(f"{clean_term}")
        return " AND ".join(processed_terms)


    def get_result(self):
        
        url = self.build_url()
        # print(f">>> {url}")
        result = requests.get(url)
        if result.status_code == 200:
            return GenomeSearchResult(ebi_search_result=result.json()).dict()
        else:
            return {"error": result.status_code, "response": result.text}


class SpeciesTaxonomyLookup(GenomeSearch):
    def __init__(self, species_taxonomy_id=""):
        self.species_taxonomy_id = species_taxonomy_id
        super().__init__(query=species_taxonomy_id)

    def format_query(self):
        return f"species_taxonomy_id:{self.species_taxonomy_id}"
