"""
.. See the NOTICE file distributed with this work for additional information
   regarding copyright ownership.
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

from fastapi import FastAPI
from ensembl_search_hub.ebi_search_app.resources.routes import router as ebi_search_router
from ensembl_search_hub.solr_search_app.resources.routes import router as solr_search_router

app = FastAPI(root_path="/api/search")
app.include_router(ebi_search_router)
app.include_router(solr_search_router)

