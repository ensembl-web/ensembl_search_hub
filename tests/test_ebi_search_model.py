import unittest

from ensembl_search_hub.ebi_search_app.models.genome_search import GenomeSearchResult


class GenomeSearchTest(unittest.TestCase):
    def setUp(self):
        self.ebi_search_mock_result = {
            "hitCount": 99,
            "entries": [
                {
                    "id": "32c15ed5-5680-4616-9989-32787a4f0264",
                    "source": "ensemblNext",
                    "fields": {
                        "common_name": ["human"],
                        "scientific_name": ["Homo sapiens"],
                        "type_type": ["population"],
                        "species_type": [],
                        "assembly": ["GRCh37"],
                        "assembly_accession": ["GCA_000001405.14"],
                        "coding_genes": ["0"],
                        "n50": ["38440852"],
                        "has_variation": ["false"],
                        "has_regulation": ["false"],
                        "annotation_provider": ["Ensembl"],
                        "annotation_method": [],
                        "genome_uuid": ["32c15ed5-5680-4616-9989-32787a4f0264"],
                        "url_name": [],
                        "tol_id": [],
                        "is_reference": ["false"],
                        "species_taxonomy_id": ["9606"],
                        "organism_id": ["31"],
                        "rank": ["0"],
                        "type_value": ["Puerto Rican in Puerto Rico"],
                    },
                }
            ],
            "facets": [],
        }

    def test_genome_search_result(self):
        genome_search_result_object = GenomeSearchResult(
            ebi_search_result=self.ebi_search_mock_result
        )
        assert type(genome_search_result_object.matches) == list
        assert type(genome_search_result_object.meta.total_count) == int

    def test_genome_search_model(self):
        genome_search_object = GenomeSearchResult(
            ebi_search_result=self.ebi_search_mock_result
        ).matches[0]
        print("===========")
        print(genome_search_object)
        assert genome_search_object.genome_id == "32c15ed5-5680-4616-9989-32787a4f0264"
        # assert genome_search_object.genome_tag == '32c15ed5-5680-4616-9989-32787a4f0264'
        assert genome_search_object.species_taxonomy_id == "9606"
        assert genome_search_object.common_name == "human"
        assert genome_search_object.scientific_name == "Homo sapiens"
        # assert genome_search_object.type == False
        assert genome_search_object.is_reference == False
        assert genome_search_object.coding_genes_count == 0
        assert genome_search_object.contig_n50 == 38440852
        assert genome_search_object.has_variation == False
        assert genome_search_object.has_regulation == False
        assert genome_search_object.annotation_provider == "Ensembl"
        # assert genome_search_object.annotation_method == 0
        assert genome_search_object.rank == 0

    def test_genome_search_assembly(self):
        genome_search_object = GenomeSearchResult(
            ebi_search_result=self.ebi_search_mock_result
        ).matches[0]
        assert genome_search_object.assembly.accession_id == "GCA_000001405.14"
        assert genome_search_object.assembly.name == "GRCh37"
        assert (
            genome_search_object.assembly.url
            == "https://www.ebi.ac.uk/ena/browser/view/GCA_000001405.14"
        )

    def test_genome_search_type(self):
        genome_search_object = GenomeSearchResult(
            ebi_search_result=self.ebi_search_mock_result
        ).matches[0]
        assert genome_search_object.type.kind == "population"
        assert genome_search_object.type.value == "Puerto Rican in Puerto Rico"

        self.ebi_search_mock_result["entries"][0]["fields"]["type_type"] = []
        self.ebi_search_mock_result["entries"][0]["fields"]["type_type"] = []
        genome_search_object = GenomeSearchResult(
            ebi_search_result=self.ebi_search_mock_result
        ).matches[0]
        assert genome_search_object.type is None
