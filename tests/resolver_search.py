import unittest, requests, json
import os
from ensembl_search_hub.main import app
from fastapi.testclient import TestClient

class TestSearchGenesAPI(unittest.TestCase):
  def setUp(self):
    self.client = TestClient(app)
    self.params = {
      "query": "ENSG00000127720",
      "genome_ids": [],
      "page": 1,
      "per_page": 1,
      "type": "Gene"
    }
    
  def test_successful_post_request(self):
    response = self.client.post("/genes", json=self.params)
    self.assertEqual(response.status_code, 200)

    response_data = response.json()
    self.assertIn("meta", response_data)
    self.assertIn("matches", response_data)
    
if __name__ == '__main__':
  unittest.main()
