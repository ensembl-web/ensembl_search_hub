FROM python:3.8

# Maintainer
LABEL org.opencontainers.image.authors="ensembl-webteam@ebi.ac.uk"

# Copy source code
COPY . /app/

# Set Work Directory
WORKDIR /app

# copy poetry toml
COPY poetry.lock pyproject.toml ./

# Install poetry and dependencies
RUN pip install poetry && \
    poetry config virtualenvs.create false && \
    poetry install --no-dev
EXPOSE 8083
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8083"]

# CMD uvicorn ensembl_search_hub.main:app --port 8083
